// Copyright Mackenzie McGehee, 2022

#include "Components/AudioComponent.h"
#include "OpenDoor.h"

// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}

// Called when the game starts
void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();
	InitialYaw = GetOwner()->GetActorRotation().Yaw;
	CurrentYaw = InitialYaw;
	DoorOpenAngle += InitialYaw;

	CheckForPressurePlate();
	FindAudioComponent();
}

// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (GetTotalMassOfActors() > MassToOpenDoors)
	{
		OpenDoor(DeltaTime);
	}
	else if (CurrentYaw != InitialYaw)
	{
		CloseDoor(DeltaTime);
	}
}

void UOpenDoor::OpenDoor(float DeltaTime)
{
	FRotator OpenDoor = GetOwner()->GetActorRotation();
	CurrentYaw = FMath::Lerp(CurrentYaw, DoorOpenAngle, DeltaTime * DoorOpenSpeed);
	OpenDoor.Yaw = CurrentYaw;
	GetOwner()->SetActorRotation(OpenDoor);

	if (!DoorAudio)
	{
		return;
	}

	if (!OpenDoorSound)
	{
		DoorAudio->Play();
		OpenDoorSound = true;
		CloseDoorSound = false;
	}

	// FRotator OpenDoor =  GetOwner()->GetActorRotation();
	// OpenDoor.Yaw = FMath::FInterpTo(OpenDoor.Yaw, DoorOpenAngle, DeltaTime, 2);
	// GetOwner()->SetActorRotation(OpenDoor);

	// FRotator OpenDoor =  GetOwner()->GetActorRotation();
	// OpenDoor.Yaw = FMath::FInterpConstantTo(OpenDoor.Yaw, DoorOpenAngle, DeltaTime, 45);
	// GetOwner()->SetActorRotation(OpenDoor);
}

void UOpenDoor::CloseDoor(float DeltaTime)
{
	FRotator CloseDoor = GetOwner()->GetActorRotation();
	CurrentYaw = FMath::Lerp(CurrentYaw, InitialYaw, DeltaTime * DoorCloseSpeed);
	CloseDoor.Yaw = CurrentYaw;
	GetOwner()->SetActorRotation(CloseDoor);

	if (!DoorAudio)
	{
		return;
	}

	if (!CloseDoorSound)
	{
		DoorAudio->Play();
		CloseDoorSound = true;
		OpenDoorSound = false;
	}
}

float UOpenDoor::GetTotalMassOfActors() const
{
	float TotalMass = 0.f;

	// Find all overlapping actors on the pressure plate
	TArray<AActor *> OverlappingActors;
	if (PressurePlate != nullptr)
	{
		PressurePlate->GetOverlappingActors(OUT OverlappingActors);
	}

	// Add up their masses
	for (AActor *Actor : OverlappingActors)
	{
		TotalMass += Actor->FindComponentByClass<UPrimitiveComponent>()->GetMass();
	}

	return TotalMass;
}

void UOpenDoor::FindAudioComponent()
{
	DoorAudio = GetOwner()->FindComponentByClass<UAudioComponent>();

	if (!DoorAudio)
	{
		UE_LOG(LogTemp, Error, TEXT("%s Missing audio component!"), *GetOwner()->GetName());
	}
}

void UOpenDoor::CheckForPressurePlate() const
{
	if (!PressurePlate)
	{
		UE_LOG(LogTemp, Error, TEXT("The %s actor does not have a PressurePlate assignment!"), *GetOwner()->GetName());
	}
}