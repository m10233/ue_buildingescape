// Copyright Mackenzie McGehee, 2022

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Engine/TriggerVolume.h"
#include "OpenDoor.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BUILDINGESCAPE_API UOpenDoor : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UOpenDoor();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	void OpenDoor(float DeltaTime);
	void CloseDoor(float DeltaTime);
	float GetTotalMassOfActors() const;
	void FindAudioComponent();
	void CheckForPressurePlate() const;

private:
	float CurrentYaw;
	float InitialYaw;

	UPROPERTY(EditAnywhere)
	float DoorOpenAngle = -90.f;
	
	UPROPERTY(EditAnywhere)
	ATriggerVolume* PressurePlate = nullptr;

	UPROPERTY(EditAnywhere)
	float DoorOpenSpeed = 1.f;	

	UPROPERTY(EditAnywhere)
	float DoorCloseSpeed = 1.f;	

	UPROPERTY(EditAnywhere)
	float MassToOpenDoors = 50.f;

	UPROPERTY()
	UAudioComponent* DoorAudio = nullptr;

	bool OpenDoorSound = false;
	bool CloseDoorSound = true;
};
